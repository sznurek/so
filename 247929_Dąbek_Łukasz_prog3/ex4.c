#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <signal.h>

void sigint_handler(int signo);
void change_size(int signo);

int main() {
    if(!isatty(STDIN_FILENO))
        exit(EXIT_FAILURE);

    signal(SIGINT, sigint_handler);
    signal(SIGWINCH, change_size);

    while(1)
        sleep(100);

    return 0;
}

void sigint_handler(int signo) {
    (void)signo;
    exit(0);
}

void change_size(int signo) {
    (void)signo;

    struct winsize size;
    ioctl(STDIN_FILENO, TIOCGWINSZ, &size);

    printf("Rows: %hd cols: %hd\n", size.ws_row, size.ws_col);
}

