#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>

// measured in pages
#define FILE_SIZE 512

void panic(const char* sc);
void touch_page(char* mem, int page);
void print_mapping(void* mem);

int pagesize;
unsigned char vec[FILE_SIZE];

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "usage: %s [file]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char* path = argv[1];
    pagesize = getpagesize();

    int fd = open(path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if(fd < 0)
        panic("creat");

    if(ftruncate(fd, pagesize * FILE_SIZE) < 0)
        panic("ftruncate");

    void* mvfile = mmap(NULL, pagesize * FILE_SIZE, PROT_WRITE, MAP_PRIVATE, fd, 0);
    if(mvfile == (void*)-1)
        panic("mmap");

    char* mfile = (char*) mvfile;

    printf("Initial mapping: \n");
    print_mapping(mvfile);

    // FUN BEGINS!

    if(madvise(mvfile, FILE_SIZE * pagesize, MADV_WILLNEED) < 0)
        perror("madvise");

    touch_page(mfile, 0);
    touch_page(mfile, 1);
    sleep(1);
    touch_page(mfile, 2);

    printf("After touching 0, 1, 2 pages with MADV_WILLNEED:\n");
    print_mapping(mvfile);


    if(madvise(mvfile, FILE_SIZE * pagesize, MADV_RANDOM) < 0)
        perror("madvise");

    touch_page(mfile, 20);
    sleep(1);
    touch_page(mfile, 40);

    printf("After touching 20, 40 pages with MADV_RANDOM:\n");
    print_mapping(mvfile);


    if(madvise(mvfile, FILE_SIZE * pagesize, MADV_SEQUENTIAL) < 0)
        perror("madvise");

    touch_page(mfile, 100);
    sleep(1);
    touch_page(mfile, 110);

    printf("After touching 100, 110 pages with MADV_SEQUENTIAL:\n");
    print_mapping(mvfile);


    if(madvise(mvfile, FILE_SIZE * pagesize, MADV_DONTNEED) < 0)
        perror("madvise");

    touch_page(mfile, 200);
    sleep(1);
    touch_page(mfile, 210);

    printf("After touching 200, 210 pages with MADV_DONTNEED:\n");
    print_mapping(mvfile);

    if(mlock(mvfile, pagesize) < 0)
        panic("mlock");

    sleep(10);

    printf("AFTER LOCK:\n");
    print_mapping(mvfile);

    // FUN ENDS :(

    munmap(mvfile, pagesize * FILE_SIZE);
    close(fd);
    unlink(path);

    return 0;
}

void print_mapping(void* mem) {
    if(mincore(mem, FILE_SIZE * pagesize, vec) < 0)
        panic("mincore");

    for(int i = 0; i < FILE_SIZE; i++)
        printf("%d", (int)vec[i]);
    printf("\n");
}

void touch_page(char* mem, int page) {
    mem[page * pagesize + 8] = '3';
}

void panic(const char* sc) {
    perror(sc);
    exit(EXIT_FAILURE);
}

