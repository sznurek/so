#include <sys/mman.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MIN_SUPERBLOCK_PAGES 16
#define MIN_BLOCK_SIZE 256

struct superblock_hdr {
    size_t pages_allocated; // real allocated pages, first block is on second page
    struct superblock_hdr* next;
    struct superblock_hdr* prev;
};

struct block_hdr {
    size_t size; // in bytes
    int is_free;
    void* next;
    void* prev;
};

static void* first_superblock = NULL;

static void* allocate_pages(size_t npages);
static void  release_pages(void* ptr, size_t npages);
static void  ensure_first_superblock(size_t npages);
static struct superblock_hdr* new_superblock(size_t npages);
static struct superblock_hdr* remove_singleton_superblocks(struct superblock_hdr* sblock);
static struct block_hdr* split_block(struct block_hdr* block, size_t size);
static void* release_superblock(void* superblock);
static void* acquire_free_block(void* superblock, size_t bytes);
static void  release_block(struct block_hdr* block);

void* malloc(size_t size) {
    size_t size_in_pages = ((size - 1) / getpagesize()) + 1;

    ensure_first_superblock(size_in_pages);

    void* current_superblock = first_superblock;
    while(current_superblock != NULL) {
        struct superblock_hdr* shdr = (struct superblock_hdr*)first_superblock;

        void* current_block = shdr->free_block;
        while(current_block != NULL) {
            struct freeblock_hdr* fhdr = (struct freeblock_hdr*) current_block;

            if(fhdr->size >= size) {
                // GOT IT!
            }

            current_block = fhdr->next_free;
        }

        current_superblock = shdr->next_superblock;
    }

    // Try allocating next superblock;
    return NULL;
}

void* calloc(size_t count, size_t size) {
    void* ptr = malloc(count * size);
    memset(ptr, 0, size * count);

    return ptr;
}

void* realloc(void* ptr, size_t size) {
    return NULL;
}

void free(void* ptr) {
    return;
}

struct superblock_hdr* remove_singleton_superblocks(struct superblock_hdr* sblock) {
    if(sblock == NULL)
        return NULL;

    struct block_hdr* first_block = (struct block_hdr*) ((char*) sblock + sizeof(struct superblock_hdr));

    if(first_block->size + sizeof(struct block_hdr) == (sblock->pages_allocated - 1) * getpagesize()) {
        struct superblock_hdr* next = sblock->next;
        release_superblock(sblock);

        return remove_singleton_superblocks(next);
    }

    sblock->next = remove_singleton_superblocks(sblock->next);
    return sblock;
}

void release_block(struct block_hdr* block) {
    if(!block->is_free)
        return;

    block->is_free = 1;
    if(block->prev && block->prev->is_free) {
        block->prev->size += block->size + sizeof(struct block_hdr);
        block->prev->next = block->next;
        block = block->prev;
    }

    if(block->next && block->next->is_free) {
        block->size += block->next->size + sizeof(struct block_hdr);
        block->next = block->next->next;
    }
}

void* acquire_free_block(void* superblock, size_t bytes) {
    struct superblock_hdr* hdr = (struct superblock_hdr*)superblock;
    struct block_hdr* block = (struct block_hdr*) ((char*) hdr + sizeof(struct superblock_hdr));

    while(block != NULL) {
        if(block->size < bytes) {
            block = fhdr->next;
            continue;
        }

        return (void*) ((char*)split_block(block, bytes) + sizeof(struct block_hdr));
    }

    return NULL;
}

struct block_hdr* split_block(struct block_hdr* block, size_t size) {
    if(size > block->size) {
        return NULL;
    }

    if(size >= block->size - sizeof(struct block_hdr) - MIN_BLOCK_SIZE) {
        block->is_free = 0;
        return block;
    } else {
        struct block_hdr* new_free = ((char*) block + sizeof(struct block_hdr) + size);
        block->is_free = 0;
        size_t old_size = block->size;

        block->size = size;
        new_free->next = block->next;
        new_free->prev = block;
        block->next = new_free;
        new_free->is_free = 1;
        new_free->size = old_size - size - sizeof(struct block_hdr);

        return block;
    }
}

struct superblock_hdr* new_superblock(size_t npages) {
    void* superblock = allocate_pages(npages + 1);
    struct superblock_hdr* hdr = (struct superblock_hdr*)superblock;

    hdr->pages_allocated = npages + 1;
    hdr->next = hdr->prev = NULL;

    struct block_hdr* fhdr = (struct block_hdr*) ((char*)hdr + getpagesize());
    fhdr->size = npages * getpagesize() - sizeof(struct block_hdr);
    fhdr->next= fhdr->prev= NULL;
    fhdr->is_free = 1;

    return hdr;
}

void* release_superblock(void* superblock) {
    struct superblock_hdr* hdr = (struct superblock_hdr*) superblock;
    release_pages(superblock, hdr->pages_allocated);
}

void ensure_first_superblock(size_t npages) {
    if(first_superblock != NULL)
        return;

    first_superblock = new_superblock(npages);
}

void* allocate_pages(size_t npages) {
    return mmap(NULL, getpagesize() * npages, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
}

void release_pages(void* ptr, size_t npages) {
    return munmap(ptr, npages * getpagesize());
}

