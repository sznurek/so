#include <stdio.h>
#include <dlfcn.h>

#define LIBRARY_PATH "./liblex1.so"

int main() {
    void* handle = dlopen(LIBRARY_PATH, RTLD_LAZY | RTLD_LOCAL);

    typedef int (*intret)(void);
    typedef int (*strlenlike)(const char*);

    intret the_answer = (intret) dlsym(handle, "the_answer");
    strlenlike mystrlen = (strlenlike) dlsym(handle, "mystrlen");

    printf("The answer: %d\n", the_answer());
    printf("Length: %d\n", mystrlen("42"));

    dlclose(handle);
    return 0;
}

