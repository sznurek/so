#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>

#define MAX_LINE 4096

char stars[MAX_LINE];
struct iovec iovecs[MAX_LINE];

void write_unix(int lines, int max_len);
void write_stdio(int lines, int max_len);
void write_scattered(int lines, int max_len);

int main(int argc, char** argv) {
    if(argc < 3) {
        fprintf(stderr, "usage: %s [lines] [max-line-len]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int lines = atoi(argv[1]);
    int max_len = atoi(argv[2]);

    memset((void*) stars, '*', max_len + 1);
    stars[max_len+1] = '\0';

    //write_unix(lines, max_len);
    write_stdio(lines, max_len);
    //write_scattered(lines, max_len);
}

void write_unix(int lines, int max_len) {
    for(int i = 0; i < lines; i++) {
        write(1, (void*) stars, (i % max_len) + 1);
    }
}

void write_stdio(int lines, int max_len) {
    if(setvbuf(stdout, NULL, _IOFBF, 4096 * 4096) != 0) {
        perror("setvbuf");
    }

    for(int i = 0; i < lines; i++) {
        fwrite((void*) stars, 1, (i % max_len) + 1, stdout);
    }
}

void write_scattered(int lines, int max_len) {
    for(int i = 0; i < max_len; i++) {
        iovecs[i].iov_base = (void*) stars;
        iovecs[i].iov_len = i + 1;
    }

    for(int i = 0; i < lines / max_len; i++) {
        writev(1, iovecs, max_len);
    }

    writev(1, iovecs, lines % max_len);
}

