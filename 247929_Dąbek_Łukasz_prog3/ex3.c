#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <signal.h>
#include <unistd.h>

void* alloc_pages(int npages);
void fill_memory(char* mem, int bytes);
void setup_signals();
void sigsegv_handler(int signo, siginfo_t* info, void* unused);

#define ALLOCATED_PAGES 64

int main() {
    setup_signals();

    char* mem = (char*)alloc_pages(ALLOCATED_PAGES);
    printf("Got memory at address %p\n", (void*) mem);
    fill_memory(mem, (ALLOCATED_PAGES - 1) * sysconf(_SC_PAGESIZE));

    mprotect(mem, sysconf(_SC_PAGESIZE), PROT_NONE);

    *(mem + 10) = 100;

    return 0;
}

void* alloc_pages(int npages) {
    return mmap(NULL, sysconf(_SC_PAGESIZE) * npages, PROT_READ | PROT_WRITE, MAP_ANON | MAP_SHARED, -1, 0);
}

void fill_memory(char* mem, int bytes) {
    static const char* wow = "WOW SO MMAPPED";

    int len = strlen(wow);
    for(int i = 0; i < bytes; i++)
        mem[i] = wow[i % len];
}

void setup_signals() {
    struct sigaction act;

    act.sa_handler = NULL;
    act.sa_sigaction = sigsegv_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    act.sa_restorer = NULL;

    sigaction(SIGSEGV, &act, NULL);
}

void sigsegv_handler(int signo, siginfo_t* info, void* unused) {
    (void)unused;
    (void)signo;

    static char buf[256];
    sprintf(buf, "%d %p\n", info->si_code, info->si_addr);
    write(1, (void*)buf, strlen(buf));

    exit(1);
}

