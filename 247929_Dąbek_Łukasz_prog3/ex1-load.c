#include <stdio.h>
#include "ex1-1.h"
#include "ex1-2.h"

// Running: $ LD_LIBRARY_PATH=. ./ex1-load
int main() {
    printf("The answer: %d\n", the_answer());
    printf("Length: %d\n", mystrlen("42"));

    return 0;
}

