#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

void list_directory(const char* path);
void stat_file(const char* path);
void print_mode(char* buffer, mode_t mode);

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "usage: %s [dir]\n", argv[0]);
        return EXIT_FAILURE;
    }

    list_directory(argv[1]);

    return 0;
}

void list_directory(const char* path) {
    char path_buffer[PATH_MAX];

    DIR* dir = opendir(path);

    if(!dir) {
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    struct dirent* entry = NULL;
    while((entry = readdir(dir)) != NULL) {
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        sprintf(path_buffer, "%s/%s", path, entry->d_name);

        if(entry->d_type == DT_DIR)
            list_directory(path_buffer);
        else if(entry->d_type != DT_LNK)
            stat_file(path_buffer);
    }

    closedir(dir);
}

void stat_file(const char* path) {
    char mode_buf[256];
    char uid_buf[256];
    char gid_buf[256];
    struct stat stat_buf;

    if(stat(path, &stat_buf) != 0) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    struct passwd* pwd = getpwuid(stat_buf.st_uid);
    struct group*  grp = getgrgid(stat_buf.st_gid);

    print_mode(mode_buf, stat_buf.st_mode);

    char* stime = ctime(&stat_buf.st_mtim);
    for(int i = 0; ; i++) {
        if(stime[i] == '\n') {
            stime[i] = 0;
            break;
        }
    }

    sprintf(uid_buf, "%d", stat_buf.st_uid);
    sprintf(gid_buf, "%d", stat_buf.st_gid);

    printf("%d %d %s %d %s %s %d %s %s\n",
            (int)stat_buf.st_ino,
            (int)stat_buf.st_blocks,
            mode_buf,
            (int)1,
            pwd ? pwd->pw_name : uid_buf,
            grp ? grp->gr_name : gid_buf,
            (int)stat_buf.st_size,
            stime,
            path);
}

void print_mode(char* buffer, mode_t mode) {
    const char* perms = "xrw";
    for(int i = 0; i < 10; i++)
        buffer[i] = '-';
    buffer[10] = 0;

    if(S_ISDIR(mode))
        buffer[0] = 'd';

    if(S_ISCHR(mode))
        buffer[0] = 'c';

    if(S_ISBLK(mode))
        buffer[0] = 'b';

    if(S_ISFIFO(mode))
        buffer[0] = 'p';

    if(S_ISLNK(mode))
        buffer[0] = 'l';

    if(S_ISSOCK(mode))
        buffer[0] = 's';

    int mask = 1;
    for(int index = 9; index >= 1; index--, mask <<= 1) {
        if((mode & mask) != 0)
            buffer[index] = perms[index%3];
    }
}

