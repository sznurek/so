#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <utime.h>

void watch_dir(const char* path);
void modify_dir(const char* path);
char* str_event(int mask);
void print_event(struct inotify_event* event);
void panic(const char* sc);

#define BUF_SIZE (sizeof(struct inotify_event) + NAME_MAX + 1)

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "usage: %s [dir]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    pid_t pid = fork();
    if(pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if(pid != 0)
        watch_dir(argv[1]);
    else
        modify_dir(argv[1]);

    return 0;
}

void watch_dir(const char* path) {
    int ifd = inotify_init();
    int wfd = inotify_add_watch(ifd, path,
            IN_ACCESS |
            IN_ATTRIB |
            IN_CLOSE_WRITE |
            IN_CLOSE_NOWRITE |
            IN_CREATE |
            IN_MODIFY |
            IN_DELETE |
            IN_DELETE_SELF |
            IN_MOVE_SELF |
            IN_MOVED_FROM |
            IN_MOVED_TO |
            IN_OPEN
            );

    if(wfd < 0) {
        perror("inotify_add_watch");
        exit(EXIT_FAILURE);
    }

    char buf[BUF_SIZE];
    while(1) {
        int len;
        if((len = read(ifd, buf, BUF_SIZE)) <= 0) {
            perror("read");
            exit(EXIT_FAILURE);
        }

        int current_pos = 0;
        while(current_pos < len) {
            struct inotify_event* event = (struct inotify_event*) &buf[current_pos];

            print_event(event);
            current_pos += sizeof(struct inotify_event) + event->len;
        }
    }

    close(ifd);
}

void modify_dir(const char* path) {
    (void)path;

    char buffer[PATH_MAX];
    char new_name[PATH_MAX];
    char read_buffer[1024];

    sleep(1);

    sprintf(buffer, "%s/%s", path, "testowyplik");
    int fd = open(buffer, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IXUSR);
    if(fd < 0)
        panic("open");

    if(write(fd, "HELLO!", strlen("HELLO!") + 1) < 0)
        panic("write");

    if(lseek(fd, 0, SEEK_SET) < 0)
        panic("lseek");

    if(read(fd, read_buffer, 2) < 0)
        panic("read");

    if(close(fd) < 0)
        panic("close");

    sprintf(new_name, "%s/%s", path, "testowyplikmove");

    if(rename(buffer, new_name) < 0)
        panic("rename");

    time_t t = time(NULL);
    struct utimbuf tb;
    tb.actime = t;
    tb.modtime = t;

    if(utime(new_name, &tb) < 0)
        panic("utime");

    if(chmod(new_name, S_IRUSR | S_IWUSR) < 0)
        panic("chmod");

    if(unlink(new_name) < 0)
        panic("unlink");

    sprintf(buffer, "%s/%s", path, "superkatalog");

    if(mkdir(buffer, S_IRUSR | S_IWUSR | S_IXUSR) < 0)
        panic("mkdir");

    if(rmdir(buffer) < 0)
        panic("rmdir");
}

void print_event(struct inotify_event* event) {
    if((event->mask & IN_ISDIR) != 0)
        printf("[DIR] ");

    printf("%s ", str_event(event->mask));
    if(event->len)
        printf("name=%s", event->name);
    printf("\n");
}

char* str_event(int mask) {
    static char buffer[1024];

    if((mask & IN_ACCESS) != 0)
        strcpy(buffer, "IN_ACCESS");

    if((mask & IN_ATTRIB) != 0)
        strcpy(buffer, "IN_ATTRIB");

    if((mask & IN_CLOSE_WRITE) != 0)
        strcpy(buffer, "IN_CLOSE_WRITE");

    if((mask & IN_CLOSE_NOWRITE) != 0)
        strcpy(buffer, "IN_CLOSE_NOWRITE");

    if((mask & IN_CREATE) != 0)
        strcpy(buffer, "IN_CREATE");

    if((mask & IN_MODIFY) != 0)
        strcpy(buffer, "IN_MODIFY");

    if((mask & IN_DELETE) != 0)
        strcpy(buffer, "IN_DELETE");

    if((mask & IN_DELETE_SELF) != 0)
        strcpy(buffer, "IN_DELETE_SELF");

    if((mask & IN_MOVE_SELF) != 0)
        strcpy(buffer, "IN_MOVE_SELF");

    if((mask & IN_MOVED_FROM) != 0)
        strcpy(buffer, "IN_MOVED_FROM");

    if((mask & IN_MOVED_TO) != 0)
        strcpy(buffer, "IN_MOVED_TO");

    if((mask & IN_OPEN) != 0)
        strcpy(buffer, "IN_OPEN");

    return buffer;
}

void panic(const char* sc) {
    perror(sc);
    exit(EXIT_FAILURE);
}

