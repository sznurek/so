#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define GNU_SOURCE
#define _XOPEN_SOURCE
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>

#define NTHREADS 16
pthread_t handles[NTHREADS];

void* thread_function(void*);

int main() {
    for(int i = 0; i < NTHREADS; i++) {
        pthread_create(&handles[i], NULL, thread_function, NULL);
    }

    system("ps -fL");
    for(int i = 0; i < NTHREADS; i++) {
        pthread_join(handles[i], NULL);
    }

    return 0;
}

void* thread_function(void* unused) {
    unused = unused;
    printf("pthread_self(): %u tid: %d\n", (unsigned int)pthread_self(), (int)syscall(SYS_gettid));
    sleep(1);

    return NULL;
}
