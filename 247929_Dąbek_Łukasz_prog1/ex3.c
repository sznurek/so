#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

#define MAX_GROUPS 1024
gid_t groups[MAX_GROUPS];

int main() {
    int ngroups;
    uid_t uid = getuid();
    gid_t gid = getgid();
    char* user = getpwuid(uid)->pw_name;
    char* group = getgrgid(gid)->gr_name;

    printf("uid=%d(%s) gid=%d(%s) groups=", uid, user, gid, group);

    ngroups = MAX_GROUPS;
    if(getgrouplist(user, gid, groups, &ngroups) == -1) {
        perror("getgrouplist");
        return -1;
    }

    for(int i = 0; i < ngroups - 1; i++) {
        printf("%d(%s),", groups[i], getgrgid(groups[i])->gr_name);
    }

    printf("%d(%s)\n", groups[ngroups-1], getgrgid(groups[ngroups-1])->gr_name);

    return 0;
}

