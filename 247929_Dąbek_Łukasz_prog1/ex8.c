#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

int fd;

void dummy(int);

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "usage: %s [file-to-read]\n", argv[0]);
        return -1;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        perror("open");
        return -1;
    }

    pid_t pid = fork();
    if(pid < 0) {
        perror("fork");
        return -1;
    }

    if(pid == 0) {
        signal(SIGUSR1, dummy);
        pause();
        printf("Position in child: %d\n", (int)lseek(fd, 0, SEEK_CUR));
        close(fd);
        exit(1);
    } else {
        char buf[128];
        if(read(fd, buf, 1) < 0) {
            perror("read");
            exit(1);
        }

        sleep(1);
        kill(pid, SIGUSR1);
        wait(NULL);

        if(fcntl(fd, F_GETFD) < 0)
            printf("File is closed.\n");
        else
            printf("File is open.\n");
    }

    return 0;
}

void dummy(int _unused) {
    _unused = _unused;
}

