#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

void yell();
void kill_by(const char* method);

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "usage: %s [exit|abort|signal]\n", argv[0]);
        return -1;
    }

    char* method = argv[1];
    atexit(yell);

    kill_by(method);

    return 0;
}

void kill_by(const char* method) {
    if(strcmp(method, "exit") == 0)
        exit(0);

    if(strcmp(method, "abort") == 0)
        abort();

    if(strcmp(method, "signal") == 0)
        kill(getpid(), SIGINT);

    printf("Unknown killing method :(\n");
    exit(1);
}

void yell() {
    printf("Exit function was called.\n");
}
