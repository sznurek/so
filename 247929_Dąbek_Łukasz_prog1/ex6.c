#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_ARGS 1024
char* args[MAX_ARGS];

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "usage: %s [cmd] [arg1] [arg2] ... [argn]\n", argv[0]);
        return -1;
    }

    int nargs = argc - 2;
    char* path = argv[1];

    args[0] = path;
    for(int i = 0; i < nargs; i++)
        args[i+1] = argv[i+2];

    pid_t pid = fork();
    if(pid < 0) {
        perror("fork");
        return -1;
    }

    if(pid == 0) {
        execve(path, args, NULL);
    } else {
        int status;
        wait(&status);

        if(WIFSIGNALED(status)) {
            printf("Process was killed by signal %d.\n", WTERMSIG(status));
        }
    }

    return 0;
}

