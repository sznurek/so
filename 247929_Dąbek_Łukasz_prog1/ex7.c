#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

extern char** environ;

void dummy_handler(int);
void print_env();

int main() {
    printf("Initial environment:\n");
    print_env();

    pid_t pid = fork();
    if(pid < 0) {
        perror("fork");
        return -1;
    }

    if(pid == 0) {
        signal(SIGUSR1, dummy_handler);
        printf("Child is sleeping...\n");
        pause();
        printf("Childs environment after pause:\n");
        print_env();
        exit(0);
    } else {
        sleep(1);
        setenv("TEST", "TEST", 1);
        chdir("/");
        kill(pid, SIGUSR1);
    }

    return 0;
}

void dummy_handler(int _unused) {
    _unused = _unused;
    return;
}

void print_env() {
    int count = 0;
    char* cwd = getcwd(NULL, 0);
    printf("CWD: %s\n", cwd);

    while(environ[count] != NULL) {
        printf("%s\n", environ[count++]);
    }

    free(cwd);
}
