#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

int main() {
    setpgid(0, 0);
    pid_t pgid = getpgid(getpid());

    struct rlimit proc_lim;
    proc_lim.rlim_cur = proc_lim.rlim_max = 50;
    setrlimit(RLIMIT_NPROC, &proc_lim);

    for(int i = 0; i < 100; i++) {
        pid_t pid = fork();
        if(pid == -1) {
            perror("fork");
            exit(-1);
        }

        if(pid == 0) {
            pause();
            exit(0);
        } else {
            sleep(1);
            killpg(pgid, SIGKILL);
        }
    }

    return 0;
}

