#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <unistd.h>
#include <signal.h>

void setup_signal_handling();
void handle_sigterm(int unused);

int main() {
    setup_signal_handling();

    while(1)
        sleep(1);

    return 0;
}

void handle_sigterm(int unused) {
    unused = unused;
    printf("SIGTERM arrived.\n");

    struct sigaction def;
    sigset_t set;

    sigemptyset(&set);

    def.sa_handler = SIG_DFL;
    def.sa_flags = 0;
    def.sa_mask = set;
    def.sa_restorer = NULL;

    sigaction(SIGTERM, &def, NULL);
}

void setup_signal_handling() {
    struct sigaction ignore, handle;
    sigset_t set;

    sigemptyset(&set);

    ignore.sa_handler = SIG_IGN;
    ignore.sa_flags = 0;
    ignore.sa_mask = set;
    ignore.sa_restorer = NULL;

    handle.sa_handler = handle_sigterm;
    handle.sa_flags = 0;
    handle.sa_mask = set;
    handle.sa_restorer = NULL;

    sigaction(SIGINT, &ignore, NULL);
    sigaction(SIGTERM, &handle, NULL);
}
