#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <unistd.h>
#include <signal.h>

int block_signal(int signum);

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "Usage: %s [prevent|create]\nIf argument is 'create' zombie will be created, otherwise zombie will not be created\n", argv[0]);
        return -1;
    }

    if(strcmp("prevent", argv[1]) == 0) {
        block_signal(SIGCHLD);
    } else if(strcmp("create", argv[1]) != 0) {
        fprintf(stderr, "Invalid argument: %s\n", argv[1]);
        return -1;
    }

    pid_t pid = fork();
    if(pid < 0) {
        perror("fork");
        return -1;
    }

    if(pid == 0)
        return 0;

    system("ps");

    return 0;
}

int block_signal(int signum) {
    struct sigaction ignore;
    sigset_t set;

    sigemptyset(&set);

    ignore.sa_handler = SIG_IGN;
    ignore.sa_flags = 0;
    ignore.sa_mask = set;
    ignore.sa_restorer = NULL;

    return sigaction(signum, &ignore, NULL);
}
