#include <stdlib.h>
#include <stdio.h>

#define _XOPEN_SOURCE
#include <unistd.h>

int main() {
    printf("pid: %d ppid:%d sid:%d pgrp:%d\n", getpid(), getppid(), getsid(getpid()), getpgrp());
    return 0;
}

