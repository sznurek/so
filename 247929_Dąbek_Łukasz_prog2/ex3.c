#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <semaphore.h>

#define NPROCESSES 4
#define NRACES 4

struct semibarrier {
    sem_t* barrier;
    sem_t* mutex;
    int barrier_fd;
    int mutex_fd;
    const char* barrier_name;
    const char* mutex_name;
    const char* cnt_name;
    int* cnt;
};

struct barrier {
    struct semibarrier* first;
    struct semibarrier* second;
};

void race(int horse, struct barrier* b);
sem_t* acquire_shared_semaphore(const char* name, int* fd);
void random_sleep(int min_us, int max_us);

struct semibarrier create_semibarrier(const char* barrier_name, const char* mutex_name, const char* cnt_name);
void init_semibarrier(struct semibarrier* sb);
void destroy_semibarrier(struct semibarrier* sb);
void wait_semibarrier(struct semibarrier* sb);

void barrier_wait(struct barrier* b);

int barrier_shm_fd, mutex_shm_fd, cnt_shm_fd;
int* cnt;
sem_t *barrier, *cnt_mutex;

int main() {
    struct barrier b;
    struct semibarrier first = create_semibarrier("/barrier1", "/mutex1", "/cnt1");
    struct semibarrier second = create_semibarrier("/barrier2", "/mutex2", "/cnt2");

    init_semibarrier(&first);
    init_semibarrier(&second);

    b.first = &first;
    b.second = &second;

    for(int i = 0; i < NPROCESSES; i++) {
        random_sleep(10, 100);
        pid_t pid = fork();
        if(pid < 0) {
            perror("fork");
            return EXIT_FAILURE;
        }

        if(pid == 0) {
            race(i, &b);
            exit(EXIT_SUCCESS);
        }
    }

    int status;
    signal(SIGCHLD, SIG_IGN);
    while(1) {
        if(wait(&status) < 0)
            break;
    }

    return 0;
}

void random_sleep(int min_us, int max_us) {
    int t = min_us + (((double)rand())/RAND_MAX) * (max_us - min_us);
    struct timespec spec;

    spec.tv_sec = t / 1000;
    spec.tv_nsec = (t % 1000) * 1000;

    nanosleep(&spec, NULL);
}

void init_semibarrier(struct semibarrier* sb) {
    if(sem_init(sb->barrier, 1, 0) < 0) {
        perror("sem_init");
        exit(EXIT_FAILURE);
    }

    if(sem_init(sb->mutex, 1, 1) < 0) {
        perror("sem_init");
        exit(EXIT_FAILURE);
    }

    cnt_shm_fd = shm_open(sb->cnt_name, O_RDWR | O_CREAT, S_IRWXU);
    if(cnt_shm_fd < 0) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    if(ftruncate(cnt_shm_fd, sizeof(int)) < 0) {
        perror("ftruncate");
        exit(EXIT_FAILURE);
    }

    cnt = (int*)mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, cnt_shm_fd, 0);
    if(cnt == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    *cnt = 0;
}

void barrier_wait(struct barrier* b) {
    wait_semibarrier(b->first);
    wait_semibarrier(b->second);
}

void destroy_semibarrier(struct semibarrier* sb) {
    shm_unlink(sb->barrier_name);
    shm_unlink(sb->mutex_name);
    shm_unlink(sb->cnt_name);

    munmap(sb->barrier, sizeof(sem_t));
    munmap(sb->mutex, sizeof(sem_t));
    munmap(sb->cnt, sizeof(int));
}

struct semibarrier create_semibarrier(const char* barrier_name, const char* mutex_name, const char* cnt_name) {
    struct semibarrier sb;

    sb.barrier = acquire_shared_semaphore(barrier_name, &sb.barrier_fd);
    sb.mutex = acquire_shared_semaphore(mutex_name, &sb.mutex_fd);
    sb.barrier_name = barrier_name;
    sb.mutex_name = mutex_name;
    sb.cnt_name = cnt_name;

    return sb;
}

void wait_semibarrier(struct semibarrier* sb) {
    int old;

    sem_wait(sb->mutex);
    old = *cnt;
    if(old != NPROCESSES - 1)
        *cnt = *cnt + 1;
    sem_post(sb->mutex);

    if(old == NPROCESSES - 1) {
        sem_post(sb->barrier);
    } else {
        sem_wait(sb->barrier);

        sem_wait(sb->mutex);
        old = *cnt;
        *cnt = *cnt - 1;
        sem_post(sb->mutex);

        if(old != 1)
            sem_post(sb->barrier);
    }
}

sem_t* acquire_shared_semaphore(const char* name, int* fd) {
    *fd = shm_open(name, O_RDWR | O_CREAT, S_IRWXU);
    if(*fd < 0) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    if(ftruncate(*fd, sizeof(sem_t)) < 0) {
        perror("ftruncate");
        exit(EXIT_FAILURE);
    }

    sem_t* sem = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED, *fd, 0);
    if(sem == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    return sem;
}

void race(int horse, struct barrier* b) {
    for(int i = 0; i < NRACES; i++) {
        printf("[%d] Horse %d started\n", i, horse);
        fflush(stdout);
        random_sleep(100, 5000);
        printf("[%d] Horse %d waiting\n", i, horse);
        fflush(stdout);
        barrier_wait(b);
    }
}

