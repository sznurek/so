#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <semaphore.h>

#define NPROCESSES 32
#define NTABLES 5

struct state {
    int eating;
    int waiting;
    int must_wait;
};

void random_sleep(int min_us, int max_us);
void ramen();
sem_t* shared_mutex(const char* path);

int state_fd;
sem_t *vars, *entry, *table;
struct state *state;

int main() {
    vars = shared_mutex("/myvars");
    table = shared_mutex("/mytable");
    entry = shared_mutex("/myentry");

    if(sem_init(vars, 1, 1) < 0 || sem_init(entry, 1, 1) < 0 || sem_init(table, 1, NTABLES) < 0) {
        perror("sem_init");
        exit(EXIT_FAILURE);
    }

    state_fd = shm_open("/mysuperstate", O_RDWR | O_CREAT, S_IRWXU);
    if(state_fd < 0) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    if(ftruncate(state_fd, sizeof(struct state)) < 0) {
        perror("ftruncate");
        exit(EXIT_FAILURE);
    }

    state = (struct state*)mmap(NULL, sizeof(struct state), PROT_READ | PROT_WRITE, MAP_SHARED, state_fd, 0);
    if(state == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    state->eating = state->waiting = state->must_wait = 0;

    for(int i = 0; i < NPROCESSES; i++) {
        random_sleep(700, 900);
        pid_t pid = fork();
        if(pid < 0) {
            perror("fork");
            return EXIT_FAILURE;
        }

        if(pid == 0) {
            ramen(i);
            exit(EXIT_SUCCESS);
        }
    }

    int status;
    signal(SIGCHLD, SIG_IGN);
    while(1) {
        if(wait(&status) < 0)
            break;
    }

    shm_unlink("/mysuperstate");

    return 0;
}

void ramen(int n) {
    while(1) {
        sem_wait(vars);
        if(state->must_wait || state->eating >= NTABLES) {
            state->waiting++;
            sem_post(vars);

            printf("[%d] Waiting for entry...\n", n);
            sem_wait(entry);

            continue;
        }

        sem_wait(table);
        printf("[%d] Got table!\n", n);

        state->eating++;
        if(state->eating == NTABLES) {
            state->must_wait = 1;
        }
        sem_post(vars);
        random_sleep(1000, 1200);
        sem_wait(vars);
        state->eating--;

        if(state->eating == 0 && state->must_wait == 1) {
            state->must_wait = 0;
            printf("[%d] Waking up waiters...\n", n);
            for(int i = 0; i < state->waiting; i++) {
                sem_post(entry);
            }

            state->waiting = 0;
        }

        sem_post(vars);

        printf("[%d] Releasing table!\n", n);
        sem_post(table);

        break;
    }
}

sem_t* shared_mutex(const char* path) {
    int fd = shm_open(path, O_RDWR | O_CREAT, S_IRWXU);
    if(fd < 0) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    if(ftruncate(fd, sizeof(sem_t)) < 0) {
        perror("ftruncate");
        exit(EXIT_FAILURE);
    }

    sem_t* s = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(s == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    return s;
}

void random_sleep(int min_us, int max_us) {
    int t = min_us + (((double)rand())/RAND_MAX) * (max_us - min_us);
    struct timespec spec;

    spec.tv_sec = t / 1000;
    spec.tv_nsec = (t % 1000) * 1000;

    nanosleep(&spec, NULL);
}

