#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>

void fatal(const char* msg);
void sendfd(int sock, int fd);
int recvfd(int sock);

int main(int argc, char** argv) {
    if(argc < 1) {
        fprintf(stderr, "usage: %s [filename]\n", argv[0]);
        return EXIT_FAILURE;
    }

    int pair[2];

    if(socketpair(PF_UNIX, SOCK_DGRAM, 0, pair) < 0)
        fatal("socketpair");

    pid_t pid = fork();
    if(pid < 0)
        fatal("fork");

    if(pid == 0) {
        char buffer[256];
        close(pair[0]);

        int fd = recvfd(pair[1]);
        if(read(fd, (void*)buffer, 1) < 0)
            fatal("error");

        printf("[%d] Read 1 byte: %d\n", getpid(), (int)buffer[0]);
    } else {
        char buffer[256];

        close(pair[1]);

        int fd = open(argv[1], O_RDONLY);
        if(fd < 0)
            fatal("open");

        if(read(fd, (void*)buffer, 1) < 0)
            fatal("read");

        printf("[%d] Read 1 byte: %d\n", getpid(), (int)buffer[0]);

        sendfd(pair[0], fd);

        int status;
        wait(&status);

        printf("Child status: %d\n", status);
    }

    return 0;
}

void fatal(const char* msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void sendfd(int sock, int fd) {
    char buffer[sizeof(struct cmsghdr) + sizeof(int)];
    struct iovec iov;
    struct msghdr msg;
    struct cmsghdr *cmsg;

    iov.iov_base = "OK";
    iov.iov_len  = 2;

    memset(&msg, 0, sizeof(msg));
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = buffer;
    msg.msg_controllen = sizeof(buffer);

    cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int));
    *(int*)CMSG_DATA(cmsg) = fd;

    msg.msg_controllen = cmsg->cmsg_len;

    if(sendmsg(sock, &msg, 0) < 0)
        fatal("sendmsg");
}

int recvfd(int sock) {
    char data[256], control[256];
    struct iovec iov;
    struct msghdr msg;
    struct cmsghdr* cmsg;

    memset(&msg, 0, sizeof(msg));
    iov.iov_base = data;
    iov.iov_len = sizeof(data) - 1;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = control;
    msg.msg_controllen = sizeof(control);

    if(recvmsg(sock, &msg, 0) < 0)
        fatal("recvmsg");

    cmsg = CMSG_FIRSTHDR(&msg);
    if(cmsg == NULL)
        fatal("cmsg is null");

    return *(int*)CMSG_DATA(cmsg);
}

