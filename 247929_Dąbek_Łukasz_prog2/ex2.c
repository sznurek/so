#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <pthread.h>

#define BATCH_SIZE 10
#define MAX_THREADS 1000
#define READER_PROBABILITY 0.5
#define MIN_WORKER_SLEEP 10  // microseconds
#define MAX_WORKER_SLEEP 100 // microseconds
#define MIN_CREATION_SLEEP 5 // microseconds
#define MAX_CREATION_SLEEP 50 // microseconds

void* reader(void* unused);
void* writer(void* unused);
void random_sleep(int min_us, int max_us);
int coin_toss(double p);
int microseconds_elapsed(struct timeval* start, struct timeval* stop);

pthread_mutex_t order_mutex,
                access_mutex,
                counter_mutex;

pthread_t threads[MAX_THREADS];

int readers_count = 0;
int writer_max_waiting = 0; // microseconds
int reader_max_waiting = 0; // microseconds

int main() {
    pthread_mutex_init(&order_mutex, NULL);
    pthread_mutex_init(&access_mutex, NULL);
    pthread_mutex_init(&counter_mutex, NULL);

    for(int i = 0; i < MAX_THREADS / BATCH_SIZE; i++) {
        for(int j = 0; j < BATCH_SIZE; j++) {
            int n = BATCH_SIZE * i + j;

            if(coin_toss(READER_PROBABILITY))
                pthread_create(&threads[n], NULL, reader, NULL);
            else
                pthread_create(&threads[n], NULL, writer, NULL);
        }

        random_sleep(MIN_CREATION_SLEEP, MAX_CREATION_SLEEP);
    }

    for(int i = 0; i < MAX_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    printf("Max reader wait: %d us\nMax writer wait: %d us\n", reader_max_waiting, writer_max_waiting);

    return 0;
}

void* reader(void* unused) {
    (void)unused;

    struct timeval start, stop;
    gettimeofday(&start, NULL);

    pthread_mutex_lock(&order_mutex);
    pthread_mutex_lock(&counter_mutex);

    if(readers_count == 0)
        pthread_mutex_lock(&access_mutex);
    readers_count++;
    pthread_mutex_unlock(&counter_mutex);
    pthread_mutex_unlock(&order_mutex);

    gettimeofday(&stop, NULL);
    int waiting_time = microseconds_elapsed(&start, &stop);
    if(waiting_time > reader_max_waiting)
        reader_max_waiting = waiting_time;

    random_sleep(MIN_WORKER_SLEEP, MAX_WORKER_SLEEP);

    pthread_mutex_lock(&counter_mutex);
    if(readers_count == 1)
        pthread_mutex_unlock(&access_mutex);
    readers_count--;
    pthread_mutex_unlock(&counter_mutex);

    return NULL;
}

void* writer(void* unused) {
    (void)unused;

    struct timeval start, stop;
    gettimeofday(&start, NULL);

    pthread_mutex_lock(&order_mutex);
    pthread_mutex_lock(&access_mutex);
    pthread_mutex_unlock(&order_mutex);

    gettimeofday(&stop, NULL);
    int waiting_time = microseconds_elapsed(&start, &stop);
    if(waiting_time > writer_max_waiting)
        writer_max_waiting = waiting_time;

    random_sleep(MIN_WORKER_SLEEP, MAX_WORKER_SLEEP);

    pthread_mutex_unlock(&access_mutex);

    return NULL;
}

int microseconds_elapsed(struct timeval* start, struct timeval* stop) {
    struct timeval res;

    timersub(stop, start, &res);
    return res.tv_sec * 1000 + res.tv_usec;
}

void random_sleep(int min_us, int max_us) {
    int t = min_us + (((double)rand())/RAND_MAX) * (max_us - min_us);
    struct timespec spec;

    spec.tv_sec = t / 1000;
    spec.tv_nsec = (t % 1000) * 1000;

    nanosleep(&spec, NULL);
}

int coin_toss(double p) {
    return ((double)rand()) / RAND_MAX <= p;
}

