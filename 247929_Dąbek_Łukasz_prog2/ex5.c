#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <pthread.h>

#define MAX_THREADS 40
#define GOODS_PRODUCED 300

void random_sleep(int min_us, int max_us);
int coin_toss(double p);
void* producer(void* unused);
void* consumer(void* unused);

pthread_t threads[MAX_THREADS];

int resources_cnt = 0;
pthread_mutex_t resources_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t resources_cond = PTHREAD_COND_INITIALIZER;

int main() {
    for(int i = 0; i < MAX_THREADS/2; i++) {
        pthread_create(&threads[i], NULL, producer, NULL);
    }

    for(int i = MAX_THREADS/2; i < MAX_THREADS; i++) {
        pthread_create(&threads[i], NULL, consumer, NULL);
    }

    for(int i = 0; i < MAX_THREADS; i++)
        pthread_join(threads[i], NULL);

    return 0;
}

void* producer(void* unused) {
    (void)unused;

    random_sleep(100, 1000);
    pthread_mutex_lock(&resources_mutex);

    resources_cnt += GOODS_PRODUCED;
    printf("Produced %d\n", GOODS_PRODUCED);
    pthread_cond_broadcast(&resources_cond);

    pthread_mutex_unlock(&resources_mutex);

    return NULL;
}

void* consumer(void* unused) {
    (void)unused;

    int to_consume = (rand() % GOODS_PRODUCED) + 1;
    random_sleep(100, 1000);
    pthread_mutex_lock(&resources_mutex);

    while(resources_cnt < to_consume)
        pthread_cond_wait(&resources_cond, &resources_mutex);

    resources_cnt -= to_consume;
    printf("Consumed %d\n", to_consume);

    pthread_mutex_unlock(&resources_mutex);

    return NULL;
}

void random_sleep(int min_us, int max_us) {
    int t = min_us + (((double)rand())/RAND_MAX) * (max_us - min_us);
    struct timespec spec;

    spec.tv_sec = t / 1000;
    spec.tv_nsec = (t % 1000) * 1000;

    nanosleep(&spec, NULL);
}

int coin_toss(double p) {
    return ((double)rand()) / RAND_MAX <= p;
}

