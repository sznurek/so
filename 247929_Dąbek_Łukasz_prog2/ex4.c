#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <mqueue.h>

#define MAX_IN 3
#define PROCESSES 8
#define NAME "/mysupersemaphore"

typedef mqd_t cs_t;

cs_t cs_open(const char* name);
void cs_close(cs_t sem, const char* name);
void cs_wait(cs_t sem);
void cs_post(cs_t sem);
void random_sleep(int min_us, int max_us);

cs_t sem;

int main() {
    sem = cs_open(NAME);
    for(int i = 0; i < MAX_IN; i++)
        cs_post(sem);

    for(int i = 0; i < PROCESSES; i++) {
        pid_t pid = fork();
        if(pid < 0) {
            perror("fork");
            exit(EXIT_FAILURE);
        }

        if(pid == 0) {
            cs_wait(sem);
            printf("IN! %d\n", i);
            random_sleep(500, 1000);
            cs_post(sem);
            printf("OUT! %d\n", i);
            exit(EXIT_SUCCESS);
        }
    }

    int status;
    while(wait(&status) >= 0);

    cs_close(sem, NAME);

    return 0;
}

void random_sleep(int min_us, int max_us) {
    int t = min_us + (((double)rand())/RAND_MAX) * (max_us - min_us);
    struct timespec spec;

    spec.tv_sec = t / 1000;
    spec.tv_nsec = (t % 1000) * 1000;

    nanosleep(&spec, NULL);
}

cs_t cs_open(const char* name) {
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 8;
    attr.mq_msgsize = 1;
    attr.mq_curmsgs = 0;

    cs_t m = mq_open(name, O_RDWR | O_CREAT, S_IRWXU, &attr);
    if(m < 0) {
        perror("mq_open");
        exit(EXIT_FAILURE);
    }

    return m;
}

void cs_close(cs_t sem, const char* name) {
    mq_close(sem);
    mq_unlink(name);
}

void cs_wait(cs_t sem) {
    char buf[1];
    if(mq_receive(sem, (char*)buf, 1, NULL) < 0) {
        perror("mq_receive");
        exit(EXIT_FAILURE);
    }
}

void cs_post(cs_t sem) {
    const char* msg = "";
    if(mq_send(sem, msg, 1, 1) < 0) {
        perror("mq_send");
        exit(EXIT_FAILURE);
    }
}

