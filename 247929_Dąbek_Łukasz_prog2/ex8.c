#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _XOPEN_SOURCE
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <poll.h>
#include <errno.h>

#define MAX_CLIENTS 1024
#define LISTEN_BACKLOG 50

#define DEBUG

#ifdef DEBUG
    #define LOG(format, args...) \
     fprintf (stderr, format , ## args)
#else
    #define LOG(format, args...)
#endif

struct buffer {
    char* data;
    int size;
    int allocated;
};

struct client {
    int fd;
    int bytes;
    int was_zero;
    int exit_after_sent;
    struct buffer* outbuf;
};

struct clients {
    int num;
    struct client client[MAX_CLIENTS];
};

struct tape {
    int num;
    int data[MAX_CLIENTS];
};

struct clients clients;
struct tape connections;
struct tape disconnections;
struct pollfd sockets[MAX_CLIENTS];

int larger_power(int n);
struct buffer* buf_create(int size);
void buf_resize(struct buffer* buf, int minsize);
void buf_append(struct buffer* buf, const char* str, int n);
void buf_pop(struct buffer* buf, int n);
void buf_free(struct buffer* buf);

void schedule_connect(int fd);
void perform_connections();

void schedule_disconnect(int i);
void perform_disconnections();

void turnoff(int sig);
void fatal(const char* msg);
int interpret(int clientnum, char* buffer, int size);

int main(int argc, char** argv) {
    if(argc < 2) {
        fprintf(stderr, "usage: %s [path]\n", argv[0]);
        return EXIT_FAILURE;
    }

    char* sock_path = argv[1];
    signal(SIGINT, turnoff);

    struct sockaddr_un my_addr;
    sockets[0].fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sockets[0].fd < 0)
        fatal("socket");

    memset(&my_addr, 0, sizeof(my_addr));
    my_addr.sun_family = AF_UNIX;
    strncpy(my_addr.sun_path, sock_path, strlen(sock_path) + 1);
    if(bind(sockets[0].fd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr_un)) < 0)
        fatal("bind");

    if(listen(sockets[0].fd, LISTEN_BACKLOG) < 0)
        fatal("listen");

    while(1) {
        sockets[0].events = POLLIN;
        int eventsnum = 0;

        for(int i = 0; i < clients.num; i++) {
            int num = i + 1;
            sockets[num].fd = clients.client[i].fd;
            sockets[num].events = POLLIN | POLLERR | POLLHUP;
            if(clients.client[i].outbuf->size > 0)
                sockets[num].events |= POLLOUT;
        }

        if((eventsnum = poll(sockets, clients.num + 1, 0)) < 0) {
            if(errno == EINTR)
                continue;
            fatal("poll");
        }

        if((sockets[0].revents & POLLIN) != 0) {
            int client = accept(sockets[0].fd, NULL, NULL); // TODO: error and EINTR
            schedule_connect(client);

            LOG("new connection (fd=%d)\n", client);
        }

        for(int i = 1; i <= clients.num; i++) {
            int cnum = i - 1;
            if((sockets[i].revents & POLLERR) != 0 || (sockets[i].revents & POLLHUP) != 0) {
                schedule_disconnect(cnum);
            } else if((sockets[i].revents & POLLIN) != 0 && clients.client[cnum].exit_after_sent == 0) {
                char buffer[1024];
                int readnum;

                readnum = read(sockets[i].fd, (void*)buffer, sizeof(buffer)); // TODO error handling
                LOG("got data (fd=%d readnum=%d)\n", sockets[i].fd, readnum);
                if(interpret(cnum, buffer, readnum) != 0) {
                    clients.client[cnum].exit_after_sent = 1;
                }
            } else if((sockets[i].revents & POLLOUT) != 0) {
                int written = write(sockets[i].fd, clients.client[cnum].outbuf->data, clients.client[cnum].outbuf->size); // TODO error handling code
                buf_pop(clients.client[cnum].outbuf, written);

                LOG("popped %d bytes (cnum=%d)\n", written, cnum);

                if(clients.client[cnum].exit_after_sent != 0 && clients.client[cnum].outbuf->size == 0) {
                    schedule_disconnect(cnum);
                }
            }
        }

        perform_disconnections();
        perform_connections();
    }

    return 0;
}

int interpret(int clientnum, char* buffer, int size) {
    struct client* c = &clients.client[clientnum];

    for(int i = 0; i < size; i++) {
        if(buffer[i] != 'a') {
            c->bytes++;
            c->was_zero = 0;
        } else if(c->was_zero) {
            LOG("got two zeros (cnum=%d)\n", clientnum);
            return 1;
        } else {
            LOG("got one zero (cnum=%d)\n", clientnum);
            char local_buf[256];
            sprintf(local_buf, "%d\n", c->bytes);
            buf_append(c->outbuf, local_buf, strlen(local_buf));
            c->was_zero = 1;
        }
    }

    return 0;
}

void schedule_connect(int fd) {
    connections.data[connections.num++] = fd;
}

void perform_connections() {
    for(int i = 0; i < connections.num; i++) {
        int fd = connections.data[i];
        int num = clients.num++;
        
        struct client* c = &clients.client[num];
        c->fd = fd;
        c->bytes = 0;
        c->was_zero = 0;
        c->exit_after_sent = 0;
        c->outbuf = buf_create(1024);

        LOG("connected (fd=%d cnum=%d)\n", fd, num);
    }

    connections.num = 0;
}

void schedule_disconnect(int i) {
    disconnections.data[disconnections.num++] = i;
}

void perform_disconnections() {
    for(int i = 0; i < disconnections.num; i++) {
        int num = disconnections.data[i];
        close(clients.client[num].fd);

        buf_free(clients.client[num].outbuf);
        clients.client[num] = clients.client[clients.num - 1];
        clients.num--;

        LOG("disconnected (cnum=%d)\n", num);
    }

    disconnections.num = 0;
}

int larger_power(int n) {
    int k = 1;
    while(k < n)
        k *= 2;

    return k;
}

struct buffer* buf_create(int size) {
    struct buffer* buf = (struct buffer*) malloc(sizeof(struct buffer));

    buf->allocated = larger_power(size);
    buf->data = (char*) malloc(buf->allocated * sizeof(char));
    buf->size = 0;

    return buf;
}

void buf_resize(struct buffer* buf, int minsize) {
    if(minsize <= buf->allocated)
        return;

    buf->allocated = larger_power(minsize);
    buf->data = (char*) realloc(buf->data, buf->allocated * sizeof(char));
}

void buf_append(struct buffer* buf, const char* str, int n) {
    buf_resize(buf, buf->size + n);
    strncpy(buf->data + buf->size, str, n);
    buf->size += n;
}

void buf_pop(struct buffer* buf, int n) {
    int new_size = buf->size - n;
    int new_allocated = larger_power(buf->size - n);
    char* new_data = (char*) malloc(new_allocated * sizeof(char));
    strncpy(new_data, buf->data + n, new_size);

    free(buf->data);
    buf->data = new_data;
    buf->size = new_size;
    buf->allocated = new_allocated;
}

void buf_free(struct buffer* buf) {
    free(buf->data);
    free(buf);
}

void fatal(const char* msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void turnoff(int sig) {
    (void)sig;
    close(sockets[0].fd);
    for(int i = 0; i < clients.num; i++)
        close(clients.client[i].fd);

    exit(EXIT_SUCCESS);
}

