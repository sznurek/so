#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#define _XOPEN_SOURCE
#include <unistd.h>

struct mypipe {
    int fd;
    pid_t pid;
};

struct mypipe mypopen(const char* cmd, const char* type);
void mypclose(struct mypipe p);

int main(int argc, char** argv) {
    if(argc < 2 || (strcmp(argv[1], "read") != 0 && strcmp(argv[1], "write") != 0)) {
        fprintf(stderr, "usage: %s [read|write]\n", argv[0]);
        return EXIT_FAILURE;
    }

    struct mypipe p;
    if(strcmp(argv[1], "write") == 0) {
        p = mypopen("/bin/cat", "w");
        if(write(p.fd, "TEST\n", 6) < 0) {
            perror("write");
            exit(EXIT_FAILURE);
        }

        sleep(1);
    } else {
        char buf[1024];
        p = mypopen("echo test", "r");

        if(read(p.fd, buf, 1024) < 0) {
            perror("read");
            exit(EXIT_FAILURE);
        }

        printf("GOT: %s\n", buf);
    }

    mypclose(p);

    return 0;
}

struct mypipe mypopen(const char* cmd, const char* type) {
    if(type[0] != 'r' && type[0] != 'w') {
        fprintf(stderr, "invalid type option\n");
        exit(EXIT_FAILURE);
    }

    int fds[2];
    if(pipe(fds) < 0) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid_t child_pid = fork();
    if(child_pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if(child_pid == 0) {
        if(type[0] == 'r') {
            if(dup2(fds[1], 1) < 0) {
                perror("dup2");
                exit(EXIT_FAILURE);
            }
        } else {
            if(dup2(fds[0], 0) < 0) {
                perror("dup2");
                exit(EXIT_FAILURE);
            }
        }

        execl("/bin/sh", "/bin/sh", "-c", cmd, (char*)NULL);
    } else {
        struct mypipe result;
        result.pid = child_pid;

        if(type[0] == 'r') {
            result.fd = fds[0];
        } else {
            result.fd = fds[1];
        }

        return result;
    }

    fprintf(stderr, "IMPOSSIBLE!\n");
    exit(EXIT_FAILURE);
}

void mypclose(struct mypipe p) {
    kill(p.pid, SIGHUP);
    close(p.fd);
}

